extends Control

onready var shader : TextureRect = $MirageShader
onready var tween : Tween = $Tween

var duration := 2
var delay := 0

func _ready():
	tween.repeat = true
	shader.modulate = Color.red
	var color_sequence := [Color(1, 0, 0, .5), Color(1, 1, 0, .5), Color(0, 1, 0, .5), Color(0, 1, 1, .5), Color(0, 0, 1, .5), Color(1, 0, 1, .5), Color(1, 0, 0, .5)]
#	var color_sequence := [Color(1, .5, .5), Color(1, 1, .5), Color(.5, 1, .5), Color(.5, 1, 1), Color(.5, .5, 1), Color(1, .5, 1), Color(1, .5, .5)]
	for i in color_sequence.size() - 1:
		tween.interpolate_property($MirageShader, "modulate", color_sequence[i], color_sequence[i + 1], duration, Tween.TRANS_LINEAR, Tween.EASE_IN, delay)
		delay += duration
	tween.start()
