extends AudioStreamPlayer

export (int) var min_delay := 1
export (int) var max_delay := 10
export (float) var random_pitch := 0.025

export (PoolStringArray) var samples

func _ready():
	randomize()
	play_sample(false)

func play_sample(delay : bool = true):
	if delay:
		yield(get_tree().create_timer(rand_range(min_delay, max_delay)), "timeout")
	stream = load(samples[randi() % samples.size()])
	pitch_scale = 1.0 + (-1 + 2 * randi() % 2) * rand_range(0.0, random_pitch)
	play()
	yield(self, "finished")
	play_sample()
