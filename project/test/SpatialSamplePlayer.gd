extends AudioStreamPlayer2D

export (int) var min_delay := 1
export (int) var max_delay := 10
export (float) var random_pitch := 0.05
export (bool) var fixed_pos := false
export (bool) var left_pos := false
export (bool) var random_pos := false

export (PoolStringArray) var samples

onready var tween : Tween = $Tween

func _ready():
	randomize()
	play_sample(false)

func play_sample(delay : bool = true):
	if delay:
		yield(get_tree().create_timer(rand_range(min_delay, max_delay)), "timeout")
	stream = load(samples[randi() % samples.size()])
	var initial_pos := Vector2((randi() % 2) * get_viewport().size.x, get_viewport().size.y / 2)
	var final_pos := Vector2(abs(initial_pos.x - get_viewport().size.x), get_viewport().size.y / 2)
	if not fixed_pos:
		tween.interpolate_property(self, "position", initial_pos, final_pos, stream.get_length(), Tween.TRANS_LINEAR, Tween.EASE_IN)
		tween.start()
	else:
		if random_pos:
			left_pos = bool(randi() % 2)
		if left_pos:
			position = Vector2(get_viewport().size.x / 4, get_viewport().size.y / 2)
		else:
			position = Vector2(3 * get_viewport().size.x / 4, get_viewport().size.y / 2)
	pitch_scale = 1.0 + (-1 + 2 * randi() % 2) * rand_range(0.0, random_pitch)
	play()
	yield(self, "finished")
	play_sample()
