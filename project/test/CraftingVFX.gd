extends Control

onready var ritual = get_parent()
onready var particles : CPUParticles2D = $Smoke
onready var texture : TextureRect = $ShockwaveShader
onready var tween : Tween = $ShockwaveShader/Tween

var leaf_scn = "res://screens/Leaf.tscn"
var yellow_leaf : String = "res://assets/visual/particle_LeafYellow.png"
var blue_leaf : String = "res://assets/visual/particle_LeafBlue.png"

var growth := 1400
var duration := 10

func setup_shockwave():
	texture.rect_scale = Vector2(1, 1)
	texture.hide()

func shockwave():
	texture.show()
	tween.interpolate_property(texture, "rect_scale", Vector2(1, 1), Vector2(growth, growth)/4, duration, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.start()

func emit_smoke_particles():
	particles.one_shot = true
	particles.restart()
	particles.emitting = true

func drop_leaves():
	randomize()
	var rand_yellow : int = 3 + randi() % 3
	var rand_blue : int = 3 + randi() % 3
	for i in range(rand_yellow):
		var instance = load(leaf_scn).instance()
		instance.texture = load(yellow_leaf)
		ritual.add_child_below_node(ritual.get_node("Characters"), instance)
	for i in range(rand_blue):
		var instance = load(leaf_scn).instance()
		instance.texture = load(blue_leaf)
		ritual.add_child_below_node(ritual.get_node("Characters"), instance)
