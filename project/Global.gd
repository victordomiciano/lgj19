extends Node

var game: Game

var recipes: Array
var recipe_mapping: Dictionary
var rings: Dictionary
var trashes: Dictionary
var all_items: Dictionary


func _init():
	recipes = RecipeList.get_list()
	create_recipe_mapping()
	rings = RingList.get_dict()
	trashes = TrashList.get_dict()
	share_tier_with_rings()
	create_all_items_list()


func create_recipe_mapping():
	var recipe_list = recipes
	recipe_mapping = {}
	for recipe in recipe_list:
		var ingredient_list = recipe.ingredient_list
		ingredient_list.sort_custom(RecipeDetector.RecipeDetectorSorter, "sort")
		recipe_mapping[ingredient_list] = recipe


func share_tier_with_rings():
	var recipe_list = recipes
	for recipe in recipe_list:
		rings[recipe.item_id].tier = recipe.tier


func create_all_items_list():
	all_items = {}
	var common_items = CommonItemList.get_dict()
	for key in common_items.keys():
		all_items[key] = common_items[key]
	for key in rings.keys():
		all_items[key] = rings[key]
	for key in trashes.keys():
		all_items[key] = trashes[key]

