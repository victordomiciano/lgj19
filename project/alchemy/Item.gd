class_name Item

var id: String
var name: String
var description: String
var image: Texture
var was_unlocked: bool
var tier

func _init(id:String, name:String, description:String, image_path:String):
	self.id = id
	self.name = name
	self.description = description
	self.was_unlocked = false
	self.tier = 0
	image = load(image_path)

