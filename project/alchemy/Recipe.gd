class_name Recipe

var item_id: String
var ingredient_list: Array
var tier: int
var was_unlocked: bool


func _init(item_id:String, ingredient_list:Array, tier:int):
	self.item_id = item_id
	self.ingredient_list = ingredient_list
	self.tier = tier
	self.was_unlocked = false
