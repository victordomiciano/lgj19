class_name GenericList

static func _get_dict_from_list(list)->Dictionary:
	var dict := {}
	for item in list:
		dict[item.id] = item
	return dict
