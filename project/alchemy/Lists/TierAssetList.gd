extends GenericList
class_name TierAssetList

static func get_list()->Array:
    return [
        load("res://assets/visual/icon_leafGray.png"),
        load("res://assets/visual/icon_leafGreen.png"),
        load("res://assets/visual/icon_leafBlue.png"),
        load("res://assets/visual/icon_leafOrange.png"),
        load("res://assets/visual/icon_leafPurple.png"),
    ]
