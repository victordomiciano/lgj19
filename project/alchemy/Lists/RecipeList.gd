extends GenericList
class_name RecipeList

# Check if IDs are valid
# Check if ingredient lists are sorted

static func get_list()->Array:
	return [
		Recipe.new("anel_de_ferro", [
			"pedras", "pedras"
		], 1),
		Recipe.new("anel_de_gosma", [
			"dejetos_quimicos", "dejetos_quimicos"
		], 1),
		Recipe.new("anel_de_plastico", [
			"dejetos_quimicos", "pedras"
		], 1),
		Recipe.new("anel_de_coquinho", [
			"gravetos", "gravetos"
		], 1),
		Recipe.new("anel_espiao", [
			"anel_de_plastico", "tralha_tecnologica"
		], 2),
		Recipe.new("anel_de_diamantes", [
			"anel_de_ferro", "pedras"
		], 2),
		Recipe.new("anel_de_ouro", [
			"anel_de_ferro", "dinheiro"
		], 2),
		Recipe.new("o_anel", [
			"anel_de_ouro", "livro_de_sebo"
		], 3),
		Recipe.new("smart_ring", [
			"anel_de_diamantes", "anel_espiao"
		], 3),
		Recipe.new("anel_lovecraftiano", [
			"anel_de_gosma", "livro_de_sebo"
		], 3),
		Recipe.new("anel_de_zanik", [
			"o_anel", "smart_ring", "anel_lovecraftiano"
		], 4),
	]
