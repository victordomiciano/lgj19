extends GenericList
class_name CommonItemList

static func get_list()->Array:
	return [
	# ID, string de nome, desc, img
		CommonItem.new("gravetos","Gravetos", "Alguns gravetos de espécies variadas de árvores, de tamanhos e espessuras variados.", "res://assets/items/tipoBase_graveto.png"),
		CommonItem.new("pedras","Pedras", "Pequenos pedaços de minérios, muito úteis para criar diversos objetos.", "res://assets/items/tipoBase_pedras.png"),
		CommonItem.new("dejetos_quimicos","Dejetos Químicos", "Um amontoado de compostos e substâncias que podem ser nocivos, mas servem de base para criar objetos, radioativos ou não.", "res://assets/items/tipoBase_dejetosQumicos.png"),
		CommonItem.new("livro_de_sebo","Livro de sebo", "Um livro antigo que não tem nenhuma relação com alquimia, mas está cheio de anotações disso.", "res://assets/items/tipoBase_livro.png"),
		CommonItem.new("tralha_tecnologica","Tralha Tecnológica", "Um aparelho revolucionário, com tecnologia de ponta, que se tornou obsoleto em 2 semanas.", "res://assets/items/tipoBase_tralhaTecnologia.png"),
		CommonItem.new("dinheiro","Dinheiro", "Pedaços de papel que provocam guerras e doenças mentais em humanos.", "res://assets/items/tipoBase_dinheiro.png"),
	]
 
static func get_dict()->Dictionary:
	return _get_dict_from_list(get_list())

