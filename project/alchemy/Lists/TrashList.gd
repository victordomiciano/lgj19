extends GenericList 
class_name TrashList

static func get_list()->Array:
	return [
		Trash.new("anel_erotico","\"Anel\" Erótico", "\"Isto é um... Anel? Mas ele é tão mole e apertado, não parece algo bom de se usar no dia a dia. Irmão Frank, o que nossos tomos humanos tem a dizer sobre isso? ..... Oh meu DEUS!!\"", "res://assets/items/tipoLixo_anelErotico.png"),
		Trash.new("cura_do_cancer","Cura do Câncer", "Se não tem gosto bom, não tem cheiro bom, não é um anel e nem serve pra fazer um, é inútil. Se é pra salvar humanos, pior ainda.", "res://assets/items/tipoLixo_curaCancer.png"),
		Trash.new("cinco_aneis","Cinco Anéis da Natureza", "Cinco anéis que surgiram e apareceu um cara azul falando sobre árvores. Jogamos eles fora antes que ele continuasse.", "res://assets/items/tipoLixo_cincoAneis.png"),
		Trash.new("donuts","Donuts", "Um objeto mágico repleto de açúcar e gordura, disponível nos sabores felicidade, alegria e picanha. Dizem que é comestível.", "res://assets/items/tipolixo_donut.png"),
		Trash.new("aneis_de_saturno","Anéis de Saturno", "Um monte de poeira, mas o Irmão Frank jura que os humanos consideram isso um tipo de anel.", "res://assets/items/tipoLixo_saturno.png"),
		Trash.new("canudo_biodegradavel","Canudo Biodegradável", "Salvaria muitas vidas marinhas, como as dos ouriços do mar, que são odiados pelos seus primos terrestres.", "res://assets/items/tipoLixo_canudo.png"),
		Trash.new("gas_alucinogeno","Gás Alucinógeno", "\"Alguma coisa... mano... não tá certo isso... que isso? ... E porque tem um macaco rosa no seu ombro?\"", "res://assets/items/tipoBase_dejetosQumicos.png"),
		Trash.new("adaga_cerimonial","Adaga Cerimonial", "Acho que esse culto não é exatamente desse tipo. Ainda.", "res://assets/items/tipoLixo_adaga.png"),
	]

static func get_dict()->Dictionary:
	return _get_dict_from_list(get_list())
