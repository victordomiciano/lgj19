extends GenericList
class_name RingList

static func get_list()->Array:
	return [
		Ring.new("anel_de_ferro","Anel de Ferro", "Você com certeza acha melhor que isso na 25 de Março. Mas ei, pelo menos esse é artesanal, então dá pra vender pelo dobro.", "res://assets/items/tipo0_anelFerro.png"),
		Ring.new("anel_de_gosma","Anel de Gosma", "Desagradável e fedido. Definição bem abstrata de anel, mas se funciona, funciona.", "res://assets/items/tipo0_anelGosma.png"),
		Ring.new("anel_de_plastico","Anel de Plástico", "Definitivamente saiu de um Kinder Ovo, ou de um pirulito. Mas do seu dedo, ele nunca mais sai.", "res://assets/items/tipo0_anelPlastico.png"),
		Ring.new("anel_de_coquinho","Anel de Coquinho", "Trançado de plantas e outros presentes da mãe natureza. Relacionado a praia e a vender sua arte.", "res://assets/items/tipo0_anelCoquinho.png"),
		Ring.new("anel_espiao","Anel Espião", "Quanto menores ficam as câmeras, menores são as chances da gente não estar sendo observado.", "res://assets/items/tipo1_anelEspiao.png"),
		Ring.new("anel_de_diamantes","Anel de Diamantes", "Muito, muito caro. Mas tão caro que é impossível descrever com palavras.", "res://assets/items/tipo1_anelDiamante.png"),
		Ring.new("anel_de_ouro","Anel de Ouro", "Feito dos melhores componentes, com uma simbologia linda - ou seria, se ouriços fossem monogâmicos pra começar.", "res://assets/items/tipo1_anelOuro.png"),
		Ring.new("o_anel","O Anel", "Pode ser desconfortável nos dedos, mas ajuda a comandar o universo.", "res://assets/items/tipo2_anelSauron.png"),
		Ring.new("smart_ring","Smart Ring", "Design elegante, milhões de funcionalidades, tecnologia de ponta. Tudo que um ouriço não dá a mínima.", "res://assets/items/tipo2_smartRing.png"),
		Ring.new("anel_lovecraftiano","Anel Lovecraftiano", "Talvez esse não seja o culto adequado para este anel. Ainda assim, magia antiga e criaturas malignas ajudam em qualquer coisa.", "res://assets/items/tipo2_anelLovecraftiando.png"),
		Ring.new("anel_de_zanik","Anel de Zanik", "A profecia se fez verdadeira, a lenda virou realidade. O grande anel de Zanik, o item mais buscado pelos ouriços desde quando não existiam videogames e os seres humanos interagiam cara a cara.", "res://assets/items/tipo4_anelZanik.png"),
	]

static func get_dict()->Dictionary:
	return _get_dict_from_list(get_list())
