class_name RecipeDetector

static func get_recipe(ingredient_list: Array):
	# Sort ingredients by ID
	var ingredient_ids = []
	for ingredient in ingredient_list:
		ingredient_ids.append(ingredient.id)
	ingredient_ids.sort()
	# Return object or null
	var key_found = null
	for key in Global.recipe_mapping.keys():
		if key == ingredient_ids:
			key_found = key
			break
	if key_found != null:
		return Global.recipe_mapping[key_found]
	return null


static func get_random_trash(ingredient_list: Array)->Trash:
	var trash_list = TrashList.get_list()
	return trash_list[int(rand_range(0,trash_list.size()-1))]


class RecipeDetectorSorter:
	static func sort(a, b):
		if a is Item:
			a = a.id
		if b is Item:
			b = b.id
		if a < b:
			return true
		return false
