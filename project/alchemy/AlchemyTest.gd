extends Node2D

func _ready():
	var common_item_list = CommonItemList.get_list()
	var ring_list = RingList.get_list()
	var trash_list = TrashList.get_list()
	print("=== COMMON ITEM")
	for item in common_item_list:
		print(item.name)
	print("=== RING")
	for item in ring_list:
		print(item.name)
	print("=== TRASH")
	for item in trash_list:
		print(item.name)
	
	print([1,2,3] == [1,2,3])
	print()
	print()
	
	var recipe1 = RecipeDetector.get_recipe([0, 2])
	var recipe2 = RecipeDetector.get_recipe([0, 1])
	var recipe3 = RecipeDetector.get_recipe([0, 1, 100])
	var recipe4 = RecipeDetector.get_recipe([0, 1, 101])
	var recipe5 = RecipeDetector.get_recipe([0, 100, 1])
	print(recipe1)
	print(recipe2)
	print(recipe3)
	print(recipe4)
	print(recipe5)

