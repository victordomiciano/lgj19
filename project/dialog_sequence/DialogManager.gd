extends Control
class_name DialogManager

signal next_dialog

onready var characters = {
	"Erinácio": [$Erinacio],
	"Steve": [$Steve, get_node("../Characters/Steve")],
	"Yvete": [$Yvete, get_node("../Characters/Yvete")],
	"Rosa": [$Rosa, get_node("../Characters/Rosa")],
	"Frank": [$Frank, get_node("../Characters/Frank")],
	"Anel": [$Rings],
}

var is_cutscene_running = false


func play_dialog_sequence(dialog_sequence, cutscene_id):
	yield(get_tree(), "idle_frame")
	is_cutscene_running = true
	set_process_input(true)
	for dialog in dialog_sequence.dialogs:
		var balloon_parent = characters[dialog.character][0]
		if characters[dialog.character].size() > 1:
			characters[dialog.character][1].play_anim("wiggle")
		show_text(balloon_parent, dialog.text)
		#Animation
		#Sound
		yield(self, "next_dialog")
		if characters[dialog.character].size() > 1:
			characters[dialog.character][1].stop_anim()
	clear_all_text();
	is_cutscene_running = false
	set_process_input(false)
	if cutscene_id == "anel_de_zanik":
		# Infinite coin spawner
		pass


func show_text(parent, message_text):
	clear_all_text()
	parent.get_node("ballon_dialogue").show()
	parent.get_node("ballon_dialogue/Label").text = message_text

func clear_all_text():
	for parents in characters.values():
		parents[0].get_node("ballon_dialogue").hide()


func _input(event):
	if (event is InputEventScreenTouch or event is InputEventMouseButton):
		if event.pressed:
			emit_signal("next_dialog")
