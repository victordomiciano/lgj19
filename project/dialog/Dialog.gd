extends Reference
class_name Dialog

var text : String
var character : String
var sound : String
var modifier

func _init(text : String, character : String, sound : String, modifier : PoolStringArray = []):
	self.text = text
	self.character = character
	self.sound = sound
	self.modifier = modifier
