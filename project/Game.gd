extends Node2D
class_name Game

export(NodePath) var hud_path = "Hud"
export(NodePath) var item_bar_path = "Hud/LowerHud/ItemBar"
export(NodePath) var cursor_holder_path = "Hud/CursorHolder"
export(NodePath) var central_area_path = "Hud/CentralArea"
export(NodePath) var alchemy_circle_path = "AlchemyRitual/AlchemyCircle"
export(NodePath) var description_balloon_path = "Hud/LowerHud/DescriptionBalloon"
export(NodePath) var dialog_manager_path = "AlchemyRitual/DialogManager"
export(NodePath) var alchemy_result_panel_path = "AlchemyResultPanel"


var hud
var item_bar: ItemBar
var cursor_holder: CursorHolder
var central_area: Control
var alchemy_circle: AlchemyCircle
var description_balloon
var dialog_manager
var alchemy_result_panel


func _ready():
	hud = get_node(hud_path)
	item_bar = get_node(item_bar_path)
	cursor_holder = get_node(cursor_holder_path)
	central_area = get_node(central_area_path)
	alchemy_circle = get_node(alchemy_circle_path)
	description_balloon = get_node(description_balloon_path)
	dialog_manager = get_node(dialog_manager_path)
	alchemy_result_panel = get_node(alchemy_result_panel_path)
	yield(get_tree(), "idle_frame")
	check_for_dialog_scenes("inicio")

func check_for_dialog_scenes(id):
	yield(get_tree(), "idle_frame")
	var cutscenes = DialogSequenceData.get_recipe_data()
	if cutscenes.has(id):
		yield(play_cutscene(cutscenes[id], id), "completed")

func play_cutscene(dialog_sequence, cutscene_id):
	hud.hide()
	yield(dialog_manager.play_dialog_sequence(dialog_sequence, cutscene_id), "completed")
	hud.show()

