extends Node2D


func _ready():
	randomize()
	var game = load("res://Game.tscn").instance()
	Global.game = game
	add_child(game)

func win():
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Main"), true)
	$Win.play()
