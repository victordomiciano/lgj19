extends Node2D

var selected_items = []
var game

func _ready():
	selected_items = []
	game = get_parent().get_parent()

func drop_item(item):
	print("DROPPED: "+item.name)
	selected_items.append(item)


func start_alchemy():
	var generated_item: Item
	var recipe : Recipe = RecipeDetector.get_recipe(selected_items)
	if recipe == null:
		generated_item = RecipeDetector.get_random_trash(selected_items)
	else:
		generated_item = Global.rings[recipe.item_id]
	print("### NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO ALCHEMY RESULT: "+generated_item.name)
	yield(game.check_for_dialog_scenes(generated_item.id), "completed")
	generated_item.was_unlocked = true
	game.item_bar.populate()
	selected_items = []

func _on_AlchemyButton_pressed():
	if selected_items.size() < 2:
		return
	start_alchemy()
