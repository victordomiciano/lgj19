extends Button

export(NodePath) var image_node_path = "MarginContainer/Image"
export(NodePath) var tier_node_path = "TierImage"

var select_sfxs = [
	"res://assets/audio/Musicas_FX/Selecionar_1.ogg",
	"res://assets/audio/Musicas_FX/Selecionar_2.ogg"
]

var item: Item

var image_node: TextureRect
var tier_node: TextureRect
var game

func _ready():
	image_node = get_node(image_node_path)
	tier_node = get_node(tier_node_path)
	game = Global.game
	print(game)


func populate(associated_item):
	while not image_node:
		yield(get_tree(), "idle_frame")
	item = associated_item
	image_node.texture = associated_item.image
	if associated_item.get("tier") != null:
		tier_node.texture = TierAssetList.get_list()[associated_item.tier]


func _on_ItemCell_button_down():
	print("HOLD: "+item.name)
	Global.game.cursor_holder.start_dragging(self)
	$AudioStreamPlayer.stream = load(select_sfxs[min(Global.game.alchemy_circle.selected_items.size(), 1)])
	$AudioStreamPlayer.play()


func _on_ItemCell_mouse_entered():
	game.description_balloon.show_description(self)


func _on_ItemCell_mouse_exited():
	game.description_balloon.hide_description()
