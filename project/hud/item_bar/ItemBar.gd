extends PanelContainer
class_name ItemBar

export(PackedScene) var item_cell_scene
export(NodePath) var cell_parent_path = "HBoxContainer"
export(int) var max_items_before_scroll = 10

var cell_parent
var create_scroll = false

func _ready():
	cell_parent = get_node(cell_parent_path)
	populate()


func populate():
	clear()
	var items_count := 0
	var common_item_list = CommonItemList.get_list()
	for item in common_item_list:
		var item_cell = item_cell_scene.instance()
		cell_parent.add_child(item_cell)
		item_cell.populate(item)
		items_count+=1
	for ring in Global.rings.values():
		if ring.was_unlocked:
			var item_cell = item_cell_scene.instance()
			cell_parent.add_child(item_cell)
			item_cell.populate(ring)
			items_count+=1
	if (not create_scroll and items_count > max_items_before_scroll):
		create_scroll = true
		cell_parent.get_parent().remove_child(cell_parent)
		$ScrollContainer.add_child(cell_parent)

func clear():
	for cell in cell_parent.get_children():
		cell_parent.remove_child(cell)
		cell.queue_free()
