extends Control

signal proceed

func show_item(item, recipe):
	yield(get_tree().create_timer(1.0), "timeout")
	$Panel/ItemPanel.texture = item.image
	if recipe != null:
		$Panel/ItemPanel/TierImage.show()
		$Panel/ItemPanel/TierImage.texture = TierAssetList.get_list()[recipe.tier]
	else:
		$Panel/ItemPanel/TierImage.hide()
	$Panel/VBoxContainer/NameLabel.text = item.name
	$Panel/VBoxContainer/DescriptionLabel.text = item.description
	show()
	yield(self, "proceed")
	hide()

func _input(event):
	if (event is InputEventScreenTouch or event is InputEventMouseButton):
		if event.pressed:
			emit_signal("proceed")


