extends Control
class_name CursorHolder

var dragging: bool

var item
var dragging_image
var game

func _ready():
	game = get_parent()
	dragging = false
	set_process(false)
	set_process_input(false)


func _process(delta):
	# Update image position
	var mouse_pos = get_viewport().get_mouse_position()
	dragging_image.rect_position = mouse_pos - dragging_image.rect_size/2


func _input(event):
	if (event is InputEventScreenTouch or event is InputEventMouseButton):
		if not event.pressed:
			# Check if release was made into alchemy area
			if game.central_area.get_global_rect().has_point(get_viewport().get_mouse_position()):
				game.alchemy_circle.drop_item(item)
			stop_dragging()


func start_dragging(cell):
	item = cell.item
	dragging_image = cell.image_node.duplicate()
	add_child(dragging_image)
	dragging_image.modulate.a = 0.7
	dragging_image.mouse_filter = MOUSE_FILTER_PASS
	dragging = true
	set_process(true)
	set_process_input(true)

func stop_dragging():
	remove_child(dragging_image)
	dragging = false
	set_process(false)
	set_process_input(false)
