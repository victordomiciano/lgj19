extends Control

const POINT_OFFSET = 10

export(NodePath) var text_label_path = "BalloonContainer/NinePatchRect/MarginContainer/VBoxContainer/Label"

var text_label


func _ready():
	text_label = get_node(text_label_path)
	hide()


func show_description(item_rect, item):
	text_label.text = item.description
	var spawn_position = Vector2(0,600)#item_rect.rect_position + vec2(item_rect.size.x, -100)#-POINT_OFFSET)
	position = spawn_position
	show()

func hide():
	hide()
