extends Control

const POINT_OFFSET = 0

export(NodePath) var text_label_path = "BalloonContainer/NinePatchRect/MarginContainer/VBoxContainer/Label"

var text_label


func _ready():
	text_label = $BalloonContainer/NinePatchRect/MarginContainer/VBoxContainer/Label #get_node(text_label_path)
	hide()


func show_description(item_cell):
	text_label.text = item_cell.item.description
	var spawn_position = item_cell.rect_global_position + Vector2(item_cell.rect_size.x/2, -POINT_OFFSET)
	rect_position = spawn_position
	show()

func hide_description():
	hide()
