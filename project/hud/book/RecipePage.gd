extends TextureButton

var open:bool

export(Array, NodePath) var recipe_column_paths

var recipe_columns


func _ready():
	open = false
	recipe_columns = []
	for recipe_column_path in recipe_column_paths:
		recipe_columns.append(get_node(recipe_column_path))


func toggle():
	if not open:
		open = true
		show()
		populate()
	else:
		open = false
		hide()


func populate():
	var recipe_list = Global.recipes
	var tiered_recipe_list = []
	for i in range(4):
		tiered_recipe_list.append([])
	# Separate in different tier lists
	for recipe in recipe_list:
		var index = recipe.tier-1
		tiered_recipe_list[index].append(recipe)
	# Send to columns
	for i in tiered_recipe_list.size():
		recipe_columns[i].populate(tiered_recipe_list[i])

