extends MarginContainer

export(bool) var show_ingredients = false
export(PackedScene) var recipe_description_scene

func populate(recipe_list):
	for child in $VBoxContainer.get_children():
		child.queue_free()
	for recipe in recipe_list:
		var recipe_desc = recipe_description_scene.instance()
		$VBoxContainer.add_child(recipe_desc)
		recipe_desc.populate(recipe, show_ingredients)

