extends HBoxContainer


var item_node
var result_node

func _ready():
	item_node = []
	item_node.append(get_node("RecipeItem"))
	item_node.append(get_node("RecipeItem2"))
	item_node.append(get_node("RecipeItem3"))
	result_node = get_node("ResultItem")


func populate(recipe, show_ingredients:bool):
	# Ignore show_ingredients for now
	var item_image_list := []
	var was_unlocked = recipe.was_unlocked
	for ingredient in recipe.ingredient_list:
		var item  = Global.all_items[ingredient]
		var image
		if not show_ingredients:
			if not was_unlocked:
				image = TierAssetList.get_list()[item.tier]
			else:
				image = item.image
		else:
			if not item.was_unlocked:
				image = item.image
			else:
				image = TierAssetList.get_list()[item.tier]
		item_image_list.append(image)
	var result_item = Global.rings[recipe.item_id]
	var result_image
	if not show_ingredients:
		if not was_unlocked:
			result_image = TierAssetList.get_list()[result_item.tier]
		else:
			result_image = result_item.image
	else:
		if was_unlocked:
			result_image = result_item.image
		else:
			result_image = TierAssetList.get_list()[result_item.tier]
	populate_items(item_image_list, result_image)


func populate_items(item_list, result):
	item_node[0].texture = item_list[0]
	item_node[1].texture = item_list[1]
	if item_list.size() > 2:
		item_node[2].texture = item_list[2]
		item_node[2].show()
		$Plus2.show()
	result_node.texture = result
