extends TextureRect
class_name AlchemyCircle

var drop_sfxs = [
	"res://assets/audio/Musicas_FX/Jogando_item_1.ogg",
	"res://assets/audio/Musicas_FX/Jogando_item_2.ogg"
]

var good_feedback = "res://assets/audio/Musicas_FX/DEU_CERTO.ogg"
var bad_feedback = "res://assets/audio/Musicas_FX/DEU_ERRADO.ogg"

var selected_items = []
var game
onready var item_spots = $ItemSpots.get_children()
onready var light_tween : Tween = $Light2D/Tween
onready var light : Light2D = $Light2D
onready var mirage : TextureRect = $Mirage/MirageShader
onready var mirage_tween : Tween = $Mirage/Tween
onready var button : TextureButton = $TextureButton
onready var crafting_vfx : Control = get_node("../CraftingVFX")


func _ready():
	clear()
	game = get_parent().get_parent()
	mirage_tween.one_shot = true
	light.color.a = 0.0
	mirage.material.set("shader_param/depth", 0.0)
	get_node("../../AlchemyButton").connect("pressed", self, "_on_AlchemyButton_pressed")


func clear():
	for item_spot in item_spots:
		item_spot.hide()
	selected_items = []


func drop_item(item):
	$Drop.stream = load(drop_sfxs[min(selected_items.size(), 1)])
	$Drop.play()
	if selected_items.size() >= 3:
		return
	print("DROPPED: "+item.name)
	item_spots[selected_items.size()].show()
	item_spots[selected_items.size()].texture = item.image
	selected_items.append(item)
	if selected_items.size() >= 2:
		activate_glow()


func start_alchemy():
	explode()
	$Explode.play()
	var generated_item: Item
	var recipe  = RecipeDetector.get_recipe(selected_items)
	if recipe == null:
		generated_item = RecipeDetector.get_random_trash(selected_items)
		$AlchemyFeedback.stream = load(bad_feedback)
		$AlchemyFeedback.play()
	else:
		generated_item = Global.rings[recipe.item_id]
		generated_item.tier = recipe.tier
		$AlchemyFeedback.stream = load(good_feedback)
		$AlchemyFeedback.play()
		print(recipe.tier)
		if recipe.tier == 4:
			get_node("/root/Main").win()
	print("### ALCHEMY RESULT: "+generated_item.name)
	clear()
	game.hud.hide()
	yield(game.alchemy_result_panel.show_item(generated_item, recipe), "completed")
	yield(game.check_for_dialog_scenes(generated_item.id), "completed")
	game.hud.show()
	generated_item.was_unlocked = true
	if recipe != null:
		recipe.was_unlocked = true
	game.item_bar.populate()
	deactivate_glow()
	print(recipe)


func activate_glow():
	crafting_vfx.setup_shockwave()
	light.show()
	light_tween.repeat = true
	light_tween.interpolate_property(light, "color:a", 0.0, 1.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	light_tween.interpolate_property(light, "color:a", 1.0, 0.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN, 1.0)
	light_tween.start()
	mirage.show()
	mirage_tween.interpolate_property(mirage.material, "shader_param/depth", 0.0, 0.001, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	mirage_tween.start()

func deactivate_glow():
	light_tween.repeat = false
	light_tween.one_shot = true
	light_tween.interpolate_property(light, "color:a", light.color.a, 0.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	light_tween.start()
	mirage_tween.interpolate_property(mirage.material, "shader_param/depth", 0.001, 0.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	mirage_tween.start()
	yield(light_tween, "tween_completed")
	light.hide()
	mirage.hide()

func explode():
	crafting_vfx.shockwave()
	crafting_vfx.emit_smoke_particles()
	crafting_vfx.drop_leaves()
	deactivate_glow()



func _on_AlchemyButton_pressed():
	if selected_items.size() < 2:
		return
	start_alchemy()