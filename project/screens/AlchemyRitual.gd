extends Control

var samples = [
	"res://sample_db/SampleDB1.tscn",
	"res://sample_db/SampleDB2.tscn",
	"res://sample_db/SampleDB3.tscn"
]

func _ready():
	add_child(load(samples[randi() % samples.size()]).instance())
