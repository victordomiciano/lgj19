extends Control

func _ready():
	$TextureButton.connect("pressed", self, "change_scene")

func change_scene():
	get_tree().change_scene_to(load("res://Main.tscn"))
