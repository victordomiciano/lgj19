extends TextureRect

var speed : float
var rotation : float

func _ready():
	randomize()
	rect_global_position = Vector2(randf() * get_viewport().size.x, -60)
	var scale = rand_range(.4, .8)
	rect_scale = Vector2(scale, scale)
	speed = rand_range(1.0, 3.0)
	rotation = rand_range(-90, 90)

func _physics_process(delta):
	rect_rotation = rotation -20 * sin(rect_global_position.y/50 + delta)
	rect_global_position = Vector2(rect_global_position.x + 1 * sin(rect_global_position.y/50 + delta), rect_global_position.y + speed)
	if rect_global_position.y > get_viewport().size.y + 60:
		queue_free()
