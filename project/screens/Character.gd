extends TextureRect

export (float) var anim_delay = 0.0
export (float) var anim_speed = 1.0

onready var anim : AnimationPlayer = $AnimationPlayer

func play_anim(anim_name):
	anim.playback_speed = anim_speed
	yield(get_tree().create_timer(anim_delay), "timeout")
	anim.play(anim_name)

func stop_anim():
	anim.stop()