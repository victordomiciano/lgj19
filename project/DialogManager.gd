extends Control
class_name DialogManager

signal next_dialog

onready var characters = {
	"Erinácio": $Erinacio,
	"Steve": $Steve,
	"Yvete": $Yvete,
	"Rosa": $Rosa,
	"Frank": $Frank,
	"Anel": $Rings,
}

var is_cutscene_running = false


func play_dialog_sequence(dialog_sequence, cutscene_id):
	yield(get_tree(), "idle_frame")
	is_cutscene_running = true
	set_process_input(true)
	for dialog in dialog_sequence.dialogs:
		var balloon_parent = characters[dialog.character]
		show_text(balloon_parent, dialog.text)
		#Animation
		#Sound
		yield(self, "next_dialog")
	clear_all_text();
	is_cutscene_running = false
	set_process_input(false)
	if cutscene_id == "anel_de_zanik":
		# Infinite coin spawner
		pass


func show_text(parent, message_text):
	clear_all_text()
	parents.get_node("ballon_dialogue").show()
	parents.get_node("ballon_dialogue/Label").text = message_text

func clear_all_text():
	for parents in characters.values():
		parents.get_node("ballon_dialogue").hide()


func _input(event):
	if (event is InputEventScreenTouch or event is InputEventMouseButton):
		if event.pressed:
			emit_signal("next_dialog")
